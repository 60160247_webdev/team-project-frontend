import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

const customerUrl = 'http://localhost:3000/customers/'
const productUrl = 'http://localhost:3000/products/'
const userUrl = 'http://localhost:3000/users/'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    customerList: [],
    productList: [],
    userList: [],
    isLogin: false
  },
  actions: {
    // =========================== Login Actions ===========================
    login ({ commit }, payload) {
      axios
        .post(userUrl + 'login', payload)
        .then(res => {
          if (res.data.accessToken) {
            localStorage.setItem('user', JSON.stringify(res.data))
            commit('loginSuccess')
          } else {
            commit('loginFail')
          }
        })
        .catch(err => {
          alert(err)
          commit('loginFail')
        })
    },
    logout ({ commit }) {
      localStorage.removeItem('user')
      commit('logout')
    },
    checkLogin ({ commit }, user) {
      if (user === null) {
        commit('loginFail')
      } else {
        commit('loginSuccess')
      }
    },
    // =========================== Customer Actions ===========================
    loadCustomerList ({ commit }) {
      axios
        .get(customerUrl)
        .then(res => commit('loadCustomerList', { res }))
        .catch(err => alert(err))
    },
    addNewCustomer ({ commit }, payload) {
      axios
        .post(customerUrl, payload)
        .then(res => commit('addNewCustomer', res.data))
        .catch(err => alert(err))
    },
    editCustomer ({ commit }, payload) {
      axios
        .put(customerUrl, payload)
        .then(res => commit('editCustomer', res.data))
        .catch(err => alert(err))
    },
    delCustomer ({ commit }, payload) {
      axios
        .delete(customerUrl + payload._id)
        .then(() => commit('delCustomer', { payload }))
        .catch(err => alert(err))
    },

    // =========================== Product Actions ===========================
    loadProductList ({ commit }) {
      axios
        .get(productUrl)
        .then(res => commit('loadProductList', { res }))
        .catch(err => alert(err))
    },
    addNewProduct ({ commit }, payload) {
      axios
        .post(productUrl, payload)
        .then(res => commit('addNewProduct', res.data))
        .catch(err => alert(err))
    },
    editProduct ({ commit }, payload) {
      axios
        .put(productUrl, payload)
        .then(res => commit('editProduct', res.data))
        .catch(err => alert(err))
    },
    delProduct ({ commit }, payload) {
      axios
        .delete(productUrl + payload._id)
        .then(() => commit('delProduct', { payload }))
        .catch(err => alert(err))
    },

    // =========================== User Actions ===========================
    loadUserList ({ commit }) {
      axios
        .get(userUrl)
        .then(res => commit('loadUserList', { res }))
        .catch(err => alert(err))
    },
    addNewUser ({ commit }, payload) {
      axios
        .post(userUrl, payload)
        .then(res => commit('addNewUser', res.data))
        .catch(err => alert(err))
    },
    editUser ({ commit }, payload) {
      axios
        .put(userUrl, payload)
        .then(res => commit('editUser', res.data))
        .catch(err => alert(err))
    },
    delUser ({ commit }, payload) {
      axios
        .delete(userUrl + payload._id)
        .then(() => commit('delUser', { payload }))
        .catch(err => alert(err))
    }
  },
  mutations: {
    // =========================== Login Mutations ===========================
    loginSuccess (state) {
      state.isLogin = true
    },
    loginFail (state) {
      state.isLogin = false
    },
    logout (state) {
      state.isLogin = false
    },
    // =========================== Customer Mutations ===========================
    loadCustomerList (state, { res }) {
      state.customerList = res.data
    },
    addNewCustomer (state, payload) {
      state.customerList.push(payload)
    },
    editCustomer (state, payload) {
      const index = state.customerList.findIndex(id => id._id === payload._id)
      state.customerList.splice(index, 1, payload)
    },
    delCustomer (state, { payload }) {
      const index = state.customerList.findIndex(id => id._id === payload._id)
      state.customerList.splice(index, 1)
    },

    // =========================== Product Mutations ===========================
    loadProductList (state, { res }) {
      state.productList = res.data
    },
    addNewProduct (state, payload) {
      state.productList.push(payload)
    },
    editProduct (state, payload) {
      const index = state.productList.findIndex(id => id._id === payload._id)
      state.productList.splice(index, 1, payload)
    },
    delProduct (state, { payload }) {
      const index = state.productList.findIndex(id => id._id === payload._id)
      state.productList.splice(index, 1)
    },

    // =========================== User Mutations ===========================
    loadUserList (state, { res }) {
      state.userList = res.data
    },
    addNewUser (state, payload) {
      state.userList.push(payload)
    },
    editUser (state, payload) {
      const index = state.userList.findIndex(id => id._id === payload._id)
      state.userList.splice(index, 1, payload)
    },
    delUser (state, { payload }) {
      const index = state.userList.findIndex(id => id._id === payload._id)
      state.userList.splice(index, 1)
    }
  },
  getters: {
    customerList: state => state.customerList,
    productList: state => state.productList,
    userList: state => state.userList,
    isLogin: state => state.isLogin,
    user: state => state.user
  }
})
